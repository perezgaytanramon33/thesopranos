document.addEventListener("DOMContentLoaded", (e) => {
  e.preventDefault();
  iniciarApp();
});
let iniciarApp = () => {
  crearGaleria();
};
let crearGaleria = () => {
  const galeria = document.querySelector(".galeria-imagenes"); //con esto seleccionarmos la clase que esta asociada al ul
  //recorrer las imagenes
  for (let i = 1; i <= 13; i++) {
    /* crear el elemento donde se va a insertar la imagen */
    const img = document.createElement("DIV");
    img.classList.add("contenido-imagenes");

    /* inyectar las imagenes al img */
    img.innerHTML = `
        <div class"modal-imagen">
            <source srcset="src/img/${i}.avif" type="image-avif" />
            <source srcset="src/img/${i}.webp" type="image-webp"  />
            <img width="200" height="300" src="src/img/${i}.webp" alt="fondo" />
        </div>
       
            <button class="contenido-boton" 
            onclick="location.href='contenido.html'">Ver Mas...</button>
        
      
      `;
    console.log(img);
    galeria.appendChild(img);
  }
};
mostrarImagen = (id) => {
  console.log(id);
};

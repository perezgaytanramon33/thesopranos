const { src, dest, watch, parallel } = require("gulp");
//css
const sass = require("gulp-sass")(require("sass")); //para usar y compilar sass
const plumber = require("gulp-plumber");
//imagenes
const webp = require("gulp-webp");
const imagemin = require("gulp-imagemin");
const cache = require("gulp-cache");
const avif = require("gulp-avif");
//para escuchar los archivos de css
css = (done) => {
  console.log("Compilando Sass");
  src("src/scss/**/*.scss") //identifica el archivo .scss a compilar
    .pipe(plumber()) //no detiene el watch
    .pipe(sass())
    .pipe(dest("build/css"));
  done();
};
function versionWebp(done) {
  const opciones = {
    calidad: 50,
  };
  src("src/img/**/*.{png,jpg,jpeg}")
    .pipe(webp(opciones))
    .pipe(dest("build/img"));
  done();
}

function versionAvif(done) {
  const opciones = {
    calidad: 50,
  };
  src("src/img/**/*.{png,jpg,jpeg}")
    .pipe(avif(opciones))
    .pipe(dest("build/img"));
  done();
}
imagenes = (done) => {
  const opciones = {
    optimizationLevel: 3,
  };
  src("src/img/**/*.{png,jpg,jpeg}")
    .pipe(cache(imagemin(opciones)))
    .pipe(dest("build/img"));
  done();
};
//para el watch
dev = (done) => {
  watch("src/scss/**/*.scss", css);
  watch("src/js/**/*.js", javascript);
  done();
};

javascript = (done) => {
  src("src/js/**/*.js").pipe(dest("build/js"));
  done();
};
exports.css = css;
exports.javascript = javascript;
exports.imagenes = imagenes;
exports.versionWebp = versionWebp;
exports.versionAvif = versionAvif;
exports.dev = parallel(imagenes, versionWebp, versionAvif, javascript, dev);
